import React, { Component } from 'react';
import axios from 'axios';

export class Counter extends Component {
  static displayName = Counter.name;

  constructor(props) {
    super(props);
    this.state = { currentCount: 0 };
    this.incrementCounter = this.incrementCounter.bind(this);
  }

    incrementCounter() {
        var formData = new FormData();
        formData.append("CustomerId", 1);
        formData.append("InitialCredit", 500);
        axios.post('api/home/create', formData).then((response) => {
            console.log(response);
        })
            .catch((error) => {
                console.log(error);
            });
  }

  render() {
    return (
      <div>
        <h1>Counter</h1>

        <p>This is a simple example of a React component.</p>

        <p aria-live="polite">Current count: <strong>{this.state.currentCount}</strong></p>

        <button className="btn btn-primary" onClick={this.incrementCounter}>Increment</button>
      </div>
    );
  }
}
