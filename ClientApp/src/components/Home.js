import React, { Component } from 'react';
import { Grid, TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import SearchIcon from '@material-ui/icons/Search';
import '../custom.css';
const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}));

export function Home(props){  
    const [customers, setCustomers] = React.useState([]);
    const [customer, setCustomer] = React.useState(null);
    const [customerInfo, setCustomerInfo] = React.useState(null);
    const [customerInfoValues, setCustomerInfoValues] = React.useState(null);
    const [value, setValue] = React.useState(null);
    const [open, setOpen] = React.useState(true);
    const classes = useStyles();    

    React.useEffect(() => {
        axios.get('/api/home')
            .then(response => {
                if (response.status === 200) {
                    const data = response.data;
                    if (data?.error === false) {
                        //There is customers, fill them
                        setCustomers(data?.success?.data?.customers);
                    }
                    else {
                        //No Customers
                        setCustomers([]);
                    }
                    setOpen(false);
                }
            })
    }, [props]);
    const handleOnClick = (event) => {                
        if (!customer) {
            alert("Select a customer first!");
        }
        else {                        
            try {
                var numVal = 0;
                if (value) {
                    numVal = Number(value);
                }                
                if (!isNaN(numVal)) {
                    axios.get(`api/home/${customer.id}/${numVal}`)
                        .then(response => {
                            if (response?.status === 200) {
                                const data = response?.data;
                                if (data) {
                                    const error = data.error;
                                    if (error === false) {
                                        alert(data.success?.data);
                                    }
                                    else {
                                        alert(error);
                                    }
                                }
                                else {
                                    alert("Error");
                                }
                            }
                            else {
                                alert("Connection Error");
                            }
                        })
                        .catch(err => {
                            setOpen(false);
                            alert("Error");
                        })
                }
                else {
                    alert("Enter a valid credit");
                }
            }
            catch (err) {
                alert("Enter a valid credit");
            }
        }
    }    
    return (
        <div>
            <Backdrop className={classes.backdrop} open={open}>
                <CircularProgress color="inherit" />
            </Backdrop>
        <h1>Hello, Dev Team!</h1>
        <p>Welcome to my World :)!</p>                    
            <Grid container direction='column' alignItems='center' justifyContent='center' spacing={4}>                
                <Grid item xs={5} container direction='column' alignItems='flex-start' justifyContent='center' spacing={2}>
                    <p>Add a Customer's Current Account</p>
                    <Grid item xs='auto'>
                        <Autocomplete
                            id="customer-list"
                            options={customers}
                            onChange={(event, value) => {
                                setCustomer(value);
                            }}
                            required
                            getOptionLabel={(option) => `[${option.id}] ${option.name} ${option.surname}`}
                            style={{ width: 300 }}
                            renderInput={(params) => <TextField {...params} label="Customer*" variant="outlined" />}
                        />
                    </Grid>
                    <Grid item xs='auto'>
                        <TextField style={{ width: 300 }}
                            onChange={(event) => {
                                const val = event.target.value;
                                setValue(val);
                            }}
                            id="outlined-basic" label="Initial Credit" variant="outlined" />
                    </Grid>
                    <Grid item xs='auto'>
                        <Button
                            variant="contained"
                            color="default"
                            onClick={handleOnClick}
                            startIcon={<AddIcon />}
                        >
                            Add
                        </Button>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Divider />
                </Grid>
                <Grid item xs={5} container direction='column' alignItems='flex-start' justifyContent='center' spacing={2}>
                    <p>Get Customer info</p>
                    <Grid item xs='auto'>
                        <Autocomplete
                            id="customer-list"
                            options={customers}
                            onChange={(event, value) => {
                                setCustomerInfo(value);
                                if (!value)
                                    setCustomerInfoValues(null);
                            }}
                            getOptionLabel={(option) => `[${option.id}] ${option.name} ${option.surname}`}
                            style={{ width: 300 }}
                            renderInput={(params) => <TextField {...params} label="Customer*" variant="outlined" />}
                        />
                    </Grid>
                    <Grid item xs='auto'>
                        <Button
                            variant="contained"
                            color="default"
                            onClick={(event) => {
                                event.preventDefault();
                                event.stopPropagation();
                                if (customerInfo) {
                                    const id = customerInfo.id;
                                    if (id) {
                                        setOpen(true);
                                        axios.get(`api/home/${id}`)
                                            .then((response) => {
                                                setOpen(false)
                                                if (response?.status === 200) {
                                                    const data = response.data;
                                                    if (data) {
                                                        if (data.error === false) {
                                                            const customerInfo = data.success?.data?.customerValue;
                                                            if (customerInfo) {
                                                                setCustomerInfoValues(customerInfo);
                                                            }
                                                        }
                                                        else {
                                                            alert(data.error);
                                                        }
                                                    }

                                                }
                                            })
                                            .catch(err => {
                                                setOpen(false)
                                                alert('Something went wrong!')
                                            })
                                    }
                                }
                                else {
                                    alert("Please select a customer!")
                                }
                            }}
                            startIcon={<SearchIcon />}
                        >
                            Search
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
            {customerInfoValues && <div style={{marginTop: '30px'}}>
                <h6>Customer Info</h6>
                <p>Id: {customerInfoValues.id}</p>
                <p>Name: {customerInfoValues.name}</p>
                <p>Surname: {customerInfoValues.surname}</p>
                <p>Balance: {customerInfoValues.balance}$</p>
                <p>Transactions:</p>
                {customerInfoValues.transactions?.length > 0 ? <div>
                    <table>
                        <tr>
                            <th>Id</th>
                            <th>Account Id</th>
                            <th>Value</th>
                        </tr>
                        {customerInfoValues.transactions?.map(transaction => {
                            return (<tr>
                                <td>{transaction.id}</td>
                                <td>{transaction.accountId}</td>
                                <td>{transaction.value}</td>
                            </tr>)
                        })}                                               
                    </table>
                </div> : <p>No transactions</p>}
            </div>}
      </div>
    );
}
