﻿using BSynchro.AppContext;
using BSynchro.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BSynchro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        /// <summary>
        /// Get all customers stored in the database
        /// </summary>
        /// <returns>List of the Customers</returns>
        [HttpGet]
        public dynamic Get()
        {
            using (var dbContext = new MyDbContext())
            {
                //Ensure database is created
                dbContext.Database.EnsureCreated();
                if (!dbContext.Customers.Any())
                {
                    return new Dictionary<string, dynamic>()
                        {
                            {"error", "No Customers, Please add a new one" },
                            {"success", false}
                        };
                }
                else
                {
                    return new Dictionary<string, dynamic>()
                        {
                            {"error", false },
                            {"success", new Dictionary<string, object>(){
                                {"data", new Dictionary<string, object>()
                                {
                                    { "customers", dbContext.Customers.ToList()}
                                }
                            } }
                        }
                    };
                }
            }
        }
        /// <summary>
        /// Get Customer's information such as Name, Surname, balance, and transactions of the accounts
        /// </summary>
        /// <param name="CustomerId">Id of the Customer</param>
        /// <returns>Customer Info</returns>
        [HttpGet("{CustomerId}")]
        public dynamic Get(int CustomerId)
        {
            if (CustomerId > 0)
            {
                using (var dbContext = new MyDbContext())
                {
                    //Ensure database is created
                    dbContext.Database.EnsureCreated();
                    if (!dbContext.Customers.Any())
                    {
                        return new Dictionary<string, dynamic>()
                        {
                            {"error", "No Customers, Please add a new one" },
                            {"success", false}
                        };
                    }
                    else
                    {
                        //There is Customers in db
                        //Check if customer exists
                        var customer = dbContext.Customers.Where(c => c.Id == CustomerId);
                        if (customer?.Count() > 0)
                        {
                            var currentCustomer = customer.FirstOrDefault();

                            List<Transaction> transactions = new List<Transaction>();
                            double balance = 0;
                            //Get Customer's accounts
                            var accounts = dbContext.Accounts.Where(account => account.CustomerId == currentCustomer.Id);
                            if(accounts.Any())
                            {
                                //Get Account transactions of customer
                                accounts.ToList().ForEach(account =>
                                {
                                    var accountTransactions = dbContext.Transactions.Where(tr => tr.AccountId == account.Id)?.ToList();
                                    if (accountTransactions.Any())
                                    {
                                        transactions.AddRange(accountTransactions);
                                        accountTransactions.ForEach(transaction => balance += transaction.Value);
                                    }
                                });
                            }
                            //Customer existed
                            return new Dictionary<string, dynamic>()
                                {
                                    {"error",  false },
                                    {"success", new Dictionary<string, object>(){
                                            { "data" , new Dictionary<string, object>(){
                                                {"customerValue", new Dictionary<string, object>(){
                                                    { "id", currentCustomer.Id },
                                                    { "name", currentCustomer.Name },
                                                    { "surname", currentCustomer.Surname },
                                                    { "balance", balance },
                                                    { "transactions", transactions},
                                                }
                                                }
                                            }
                                            }
                                        }
                                }
                                };
                        }
                        else
                        {
                            //Customer not existed
                            return new Dictionary<string, dynamic>()
                                {
                                    {"error", "Invalid Customer" },
                                    {"success", false}
                                };
                        }
                    }
                }
            }
            return new Dictionary<string, dynamic>()
                        {
                            {"error", "Please enter Customer ID" },
                            {"success", false}
                        };
        }
        /// <summary>
        /// Create new account for the customer given his ID, if InitialCredit greater than zero, then a transaction will be sent to the new account.
        /// </summary>
        /// <param name="CustomerId">Customer ID</param>
        /// <param name="InitialCredit">Initial Credit to be added as an account transaction</param>
        /// <returns>State of the operation</returns>
        [HttpGet("{CustomerId}/{InitialCredit}")]
        public dynamic CreateAccount(int CustomerId, double InitialCredit)
        {
            if(CustomerId > 0)
            {
                using (var dbContext = new MyDbContext())
                {
                    //Ensure database is created
                    dbContext.Database.EnsureCreated();
                    if (!dbContext.Customers.Any())
                    {
                        return new Dictionary<string, dynamic>()
                        {
                            {"error", "No Customers, Please add a new one" },
                            {"success", false}
                        };                        
                    }
                    else
                    {
                        //There is Customers in db
                        //Check if customer exists
                        var customer = dbContext.Customers.Where(c => c.Id == CustomerId);
                        if(customer?.Count() > 0)
                        {
                            //Customer exists
                            //Create new account 
                            int lastAccountId = 1;
                            if (dbContext.Accounts.Any())
                            {
                                lastAccountId = dbContext.Accounts.Count() + 1;
                            }                            
                            dbContext.Accounts.Add(new Models.Account() { Id = lastAccountId, CustomerId = CustomerId });

                            if ( InitialCredit != 0)
                            {
                                int lastTransactionId = 1;
                                if (dbContext.Transactions.Any())
                                {
                                    lastTransactionId = dbContext.Transactions.Count() + 1;
                                }
                                //Initiate the created account with a balance                                
                                dbContext.Transactions.Add(new Models.Transaction() {Id= lastTransactionId, AccountId= lastAccountId, Value= InitialCredit });
                            }
                            dbContext.SaveChanges();
                            //Customer existed
                            return new Dictionary<string, dynamic>()
                                {
                                    {"error",  false },
                                    {"success", new Dictionary<string, object>(){
                                            { "data" , "Account Created Successfully"
                                            }
                                        }
                                }
                                };
                        }
                        else
                        {
                            //Customer not existed
                            return new Dictionary<string, dynamic>()
                                {
                                    {"error", "Invalid Customer" },
                                    {"success", false}
                                };
                        }
                    }
                }
            }
            return new Dictionary<string, dynamic>()
                        {
                            {"error", "Please enter Customer ID" },
                            {"success", false}
                        };
        }
    }
}
