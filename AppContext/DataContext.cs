﻿using BSynchro.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BSynchro.AppContext
{
    public class MyDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=TestDatabase.db", options =>
            {
                options.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName);
            });
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Map table names
            modelBuilder.Entity<Customer>().ToTable("Customers", "test");
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Id);                                
            });
            modelBuilder.Entity<Account>().ToTable("Accounts", "test");
            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(e => e.Id);
            });
            modelBuilder.Entity<Transaction>().ToTable("Transactions", "test");
            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.HasKey(e => e.Id);
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}
