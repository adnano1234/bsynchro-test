using BSynchro.AppContext;
using BSynchro.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BSynchro
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();          
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();                                        
                    using (var dbContext = new MyDbContext())
                    {
                        //Ensure database is created
                        dbContext.Database.EnsureCreated();
                        if (!dbContext.Customers.Any())
                        {
                            dbContext.Customers.AddRange(new Customer[]
                                {
                             new Customer{ Id=1, Name="Adnan", Surname="Awwad"},
                             new Customer{ Id=2, Name="Yussef", Surname="Baker"},
                             new Customer{ Id=3, Name="Fouad", Surname="Khodr"}
                                });
                            dbContext.Accounts.AddRange(new Account[]
                                {
                                    new Account{ Id=1, CustomerId=1},
                                    new Account{ Id=2, CustomerId=3},
                                    new Account{ Id=3, CustomerId=2},
                                    new Account{ Id=4, CustomerId=2},
                                });
                            dbContext.Transactions.AddRange(new Transaction[]
                                {
                                    new Transaction{ Id=1, AccountId=1, Value=1700},
                                    new Transaction{ Id=2, AccountId=2, Value=-1200},
                                    new Transaction{ Id=3, AccountId=1, Value=100},
                                    new Transaction{ Id=4, AccountId=4, Value=-1500},
                                    new Transaction{ Id=5, AccountId=2, Value=800},
                                    new Transaction{ Id=6, AccountId=2, Value=-900},
                                    new Transaction{ Id=7, AccountId=4, Value=1500},
                                    new Transaction{ Id=8, AccountId=3, Value=-800},
                                    new Transaction{ Id=9, AccountId=4, Value=900}
                                });
                            dbContext.SaveChanges();
                        }                        
                    }
                });
    }
}
