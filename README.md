# BSynchro Test



## Getting started

This project consists of a simple UI to designate how to fetch Customer Info and add Customer Current Account

## APIs

- GET /api/home: Get Customers
- GET /api/home/[id] : Get Customer Information
- GET /api/home/[id]/[initialCredit]: Add New account to the Customer specified, if initial credit other than 0, then a new transaction will be commited for the create account

## Test and Deploy
Just Clone the repo, build the app in Visual Studio or VS Code, then run the project in the IIS Server or any supported one.
The Database used is SQLite attached locally to the project under the name 'TestDatabase.db'


