﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSynchro.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }        

    }
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public double InitialCredit { get; set; }
    }
}
